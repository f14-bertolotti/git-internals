
package core;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.zip.DataFormatException;

public interface GitObject {
    public String getHash() throws FileNotFoundException, IOException;
    public String getType() throws IOException, DataFormatException;
}
