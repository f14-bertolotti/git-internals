package core;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.Inflater;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;
import java.util.List;
import java.util.ArrayList;


class GitCommitObject implements GitObject {
    /*
      manages basic git's commit operations
    */

    private String repoPath;
    private String commitHash;
    private String gitType;
    private File commit;
    private File master;
    
    public GitCommitObject(String repoPath, String commitHash) {
	// initialize the path to the repo
	// initialize the hash of the commit
	// initialize File to the master label
	// initialize File to the commit
	this.repoPath = repoPath;
	this.commitHash = commitHash;
	this.gitType = "commit";
	this.master = new File(repoPath+"/refs/heads/master");
	this.commit = new File(repoPath+"/objects/"+
			       commitHash.substring(0,2)+"/"+
			       commitHash.substring(2));
    }

    public String getHash() throws FileNotFoundException, IOException{
	return commitHash;
    }

    public String getType() {
	return gitType;
    }
    
    public String getTreeHash() throws FileNotFoundException, IOException{
	// gets the hash of the tree file associated to this commit
	String result = new String("");
	FileInputStream commitInputStream = new FileInputStream(commit);
	InputStream commitInflatedStream = new InflaterInputStream(commitInputStream);

	for(int i = 0; i < 2; ++i) // skips to the relevant info
	    while((char)commitInflatedStream.read() != ' '){}

	do { // actually reads the tree hash
	    result += (char)commitInflatedStream.read();
	}while(result.charAt(result.length()-1) != '\n');
	return result.substring(0,result.length()-1);
    }

    public String getParentHash()  throws FileNotFoundException, IOException{
	// returns the parent hash of the commit
	String result = new String("");
	FileInputStream commitInputStream = new FileInputStream(commit);
	InputStream commitInflatedStream = new InflaterInputStream(commitInputStream);

	for(int i = 0; i < 3; ++i) // skips to the relevant info
	    while((char)commitInflatedStream.read() != ' '){}

	do { // actually reads the parent hash
	    result += (char)commitInflatedStream.read();
	}while(result.charAt(result.length()-1) != '\n');
	return result.substring(0,result.length()-1);
    }

    public String getAuthor()  throws FileNotFoundException, IOException{
	// returns author of the commit
	String result = new String("");
	FileInputStream commitInputStream = new FileInputStream(commit);
	InputStream commitInflatedStream = new InflaterInputStream(commitInputStream);

	for(int i = 0; i < 4; ++i) // skips to the relevant info
	    while((char)commitInflatedStream.read() != ' '){}

	do { // actually reads the author
	    result += (char)commitInflatedStream.read();
	}while(result.charAt(result.length()-1) != '>');
	return result; 
    }

}
