
package core;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.Inflater;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;
import java.util.List;
import java.util.ArrayList;


class GitRepository {

    /*
      manages basic git's repo operations
    */
    
    private String repoPath;
    private File HEAD;
    
    public GitRepository(String repoPath) {
	// initialize the path to the repo and
	// initialize HEAD file
	this.repoPath = repoPath;
	this.HEAD = new File(repoPath+"/HEAD");
    }

    public String getHeadRef() throws FileNotFoundException, IOException{
	// reads the HEAD file
	FileReader fileReader = new FileReader(HEAD); 
	BufferedReader bufferedReader = new BufferedReader(fileReader);
	String refs = bufferedReader.readLine().substring(5);
	return refs;

    }

    public String getRefHash(String masterLabelPath) throws FileNotFoundException, IOException{
	// return the String of the hash relative to master
	FileReader fileReader = new FileReader(repoPath+"/"+masterLabelPath);
	BufferedReader bufferedReader = new BufferedReader(fileReader);
	String hash = bufferedReader.readLine();
	return hash;
	
    }
}
