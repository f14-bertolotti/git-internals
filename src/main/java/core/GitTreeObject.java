package core;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.Inflater;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;
import java.util.List;
import java.util.ArrayList;

class GitTreeObject implements GitObject {
    /*
      manages basic operation on git's tree object
    */
    private String repoPath;
    private String masterTreeHash;
    private File tree;
    private boolean once;
    
    private String gitType;
    private List<String> names;
    private List<String> hashes;
    
    public GitTreeObject(String repoPath, String masterTreeHash) {
	// initialize generico infos
	this.repoPath = repoPath;
	this.masterTreeHash = masterTreeHash;
	this.tree = new File(repoPath+"/objects/"+
			     masterTreeHash.substring(0,2)+"/"+
			     masterTreeHash.substring(2));
	this.gitType = "tree";
	this.names = new ArrayList<String>();
	this.hashes = new ArrayList<String>();
	this.once = false;
    }

    private void retrieveInfoOnce() throws FileNotFoundException, IOException {
	// gets executed only one time and exctract general info
	// about the tree object
	
	if(once == true) return;
	once = true;

	FileInputStream treeInputStream = new FileInputStream(tree);
	InputStream treeInflatedStream = new InflaterInputStream(treeInputStream);
	String result = new String(); // keeps the full inflated string
	String tmp = new String(); // keeps hashes

	for(int read = 0; read != -1; read=treeInflatedStream.read()) {
	    result += (char)read;
	    if(read < 16) tmp+="0"; // transform hex like 7 in 07
	    tmp += Integer.toHexString(read);
	    if(read == 0) {
		if(tmp.length() >= 40) {
		    hashes.add(tmp.substring(0,40));
		}
		tmp = "";
            }
	}
	
	if(tmp.length() >= 40) hashes.add(tmp.substring(0,40)); // adds last hash

      	String[] tokens = result.split("[ \0]");
	gitType = tokens[1];
	for(int i = 3; i < tokens.length; ++i)
	    if(i%2==0) names.add(tokens[i]); // skips hash+permissions tokens
	
    }

    private String getGitTypeFromFile(String hash) throws FileNotFoundException, IOException{
	// reads only the type based on a hash filename
	File entryFile = new File(repoPath+"/objects/"+hash.substring(0,2)+"/"+hash.substring(2));
	FileInputStream treeInputStream = new FileInputStream(entryFile);
	InputStream treeInflatedStream = new InflaterInputStream(treeInputStream);
	String entryType = new String();
	for(int read = 1; (char)read != ' '; read = treeInflatedStream.read())
	    entryType += (char)read;
	return entryType.substring(1);
    }
    
    
    public String getHash() throws FileNotFoundException, IOException{
	// returns hash associated to this
	retrieveInfoOnce();
	return masterTreeHash;
    }

    public String getType()  throws FileNotFoundException, IOException{
	// returns the tye.
	return gitType;
    }
    
    public List<String> getEntryPaths() throws FileNotFoundException, IOException{
	// returns all the names in the tree
	retrieveInfoOnce();
	return names;
    }

    public GitObject getEntry(String entryName) throws FileNotFoundException, IOException{
	// returns GitTreeObject or GitBlobObject based on entryName
	retrieveInfoOnce();

	String hash = hashes.get(names.indexOf(entryName)); // gets hash of entryType
	String entryType = this.getGitTypeFromFile(hash); // retrieve git type of the hash

	if(entryType.equals("tree")) return new GitTreeObject(repoPath, hash);	
	if(entryType.equals("blob")) return new GitBlobObject(repoPath, hash);
       
        return null;
    }
    
}
