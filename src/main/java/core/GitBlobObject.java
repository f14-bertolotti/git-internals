
package core;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.Inflater;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;
import java.util.List;
import java.util.ArrayList;


class GitBlobObject implements GitObject {
    /*
      manages basic git's blob operations
    */

    private String hash;
    private String repoPath;
    private File blob;
    private String gitType;
    
    public GitBlobObject(String repoPath, String hash) throws IOException {
	// initialize the path to the repo
	// initialize the hash of a git object
	// initialize the file relative to the blob
	this.repoPath = repoPath;
	this.gitType = "blob";
	this.hash = hash;
	this.blob = new File(repoPath+"/objects/"+
			     hash.substring(0,2)+"/"+hash.substring(2));
    }

    public String getHash() throws FileNotFoundException, IOException{
	return hash;
    }

    public String getType() throws IOException, DataFormatException {
	return gitType;
    }

    public String getContent() throws IOException, DataFormatException {
	// reads bytes after 0x00 marker which limits metadata
	FileInputStream blobInputStream = new FileInputStream(blob);
	InputStream blobInflatedStream = new InflaterInputStream(blobInputStream);
	String result = new String();

	while(blobInflatedStream.read() != 0x00){} //skips to relevant infos

	do {
	    result += (char)blobInflatedStream.read();
	} while(result.charAt(result.length()-1) != (char)-1);	    
	
	return result.substring(0,result.length()-1);
    }
    
}
